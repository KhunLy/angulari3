import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-exo1',
  templateUrl: './exo1.component.html',
  styleUrls: ['./exo1.component.scss']
})
export class Exo1Component implements OnInit {

  nombre = 42.421;
  chaine = 'HeLlo WorLd !!';
  bool = false;
  date = new Date();
  liste = ['sel', 'poivre', 'sucre'];

  cardVisible = true;

  constructor() { }

  ngOnInit(): void {
  }

  toggleCard() {
    //alert();
    this.cardVisible = !this.cardVisible;
  }

}
