import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  myName = 'Khun';

  items = [
    { title: 'Accueil', icon: 'home', link: '/home' },
    { title: 'A propos', icon: 'star', link: '/about' },
    { title: 'Exercices', icon: 'book', children: [
      { title: 'Exercice 1', link: '/exo1' }
    ] },
  ]
}
